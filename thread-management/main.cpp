#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds interval)
{
    string text = "Hello threads...";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "End of THD#" << id << endl;
}

class BackgroundWork
{
    const int id_;
public:
    BackgroundWork(int id) : id_{id}
    {}

    void operator()(const string& text, chrono::milliseconds interval)
    {
        for(const auto& c : text)
        {
            cout << "BW#" << id_ << ": " << c << endl;
            this_thread::sleep_for(interval);
        }

        cout << "End of BW#" << id_ << endl;
    }
};

template <typename Container>
void print(const Container& c, const string& prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : c)
        cout << item << " ";
    cout << "]" << endl;
}

int main()
{
    thread thd0;

    cout << thd0.get_id() << endl;

    thread thd1{&background_work, 1, 200ms};
    thread thd2{&background_work, 2, chrono::milliseconds(400) };

    BackgroundWork bw3{3};
    thread thd3{bw3, "concurrent", 200ms};
    thread thd4{BackgroundWork{4}, "threads", 300ms};

    const vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 9 };
    vector<int> target1(data.size());
    vector<int> target2(data.size());

    thread thd5 { [&data, &target1] { copy(data.begin(), data.end(), target1.begin()); } };
    thread thd6 { [&data, &target2] { copy(data.begin(), data.end(), target2.begin()); } };

    thd1.join();
    assert(!thd1.joinable());

    thd2.detach();
    assert(!thd2.joinable());

    thd3.join();
    thd4.join();

    thd5.join();
    thd6.join();

    print(target1, "target1");
    print(target2, "target2");
}
