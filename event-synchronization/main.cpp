#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <mutex>
#include <atomic>
#include <condition_variable>

using namespace std;

namespace EventsSyncWithAtomic
{

class Data
{
    vector<int> data_;
    atomic<bool> is_ready_{};
public:
    void read()
    {
        cout << "Start reading..." << endl;
        this_thread::sleep_for(2s);

        data_.resize(100);
        generate(data_.begin(), data_.end(), [] { return rand() % 100; });

        is_ready_.store(true, memory_order::memory_order_release);
    }

    void process(int id)
    {
        while (!is_ready_.load(memory_order::memory_order_acquire))
        {
        }

        // processing
        long sum = accumulate(data_.begin(), data_.end(), 0L);

        cout << "Id: " << id << "; Sum: " << sum << endl;
    }
};

}


namespace EventsWithCv
{
    class Data
    {
        vector<int> data_;
        bool is_ready_ = false;
        condition_variable cv_data_ready_;
        mutex mtx_;

    public:
        void read()
        {
            cout << "Start reading..." << endl;
            this_thread::sleep_for(2s);

            data_.resize(100);
            generate(data_.begin(), data_.end(), [] { return rand() % 100; });

            unique_lock<mutex> lk{mtx_};
            is_ready_ = true;
            lk.unlock();

            cv_data_ready_.notify_all();
        }

        void process(int id)
        {
            unique_lock<mutex> lk{mtx_};

            cv_data_ready_.wait(lk, [this] { return is_ready_; });

            lk.unlock();

            // processing
            long sum = accumulate(data_.begin(), data_.end(), 0L);

            cout << "Id: " << id << "; Sum: " << sum << endl;
        }

    };
}

namespace Singletons
{
    inline namespace Meyers
    {
        class Singleton
        {
            Singleton() { cout << "Singleton()" << endl;}
            ~Singleton() { cout << "~Singleton()" << endl;}
        public:
            Singleton(const Singleton&) = delete;
            Singleton& operator=(const Singleton&) = delete;

            void do_stuff()
            {}

            static Singleton& instance()
            {
                static Singleton unique_instance;

                return unique_instance;
            }
        };
    }

    namespace WithAtomicPointer
    {

        class Singleton
        {
            static atomic<Singleton*> instance_;
            static mutex mtx_;

            Singleton() {};
        public:
            Singleton(const Singleton&) = delete;
            Singleton& operator=(const Singleton&) = delete;

            void do_stuff()
            {}

            static Singleton& instance()
            {
                if (!instance_.load())
                {
                    lock_guard<mutex> lk{mtx_};
                    if (!instance_.load())
                    {
                        //instance_ = new Singleton();
                        // 1 - alloc
                        void* raw_mem = ::operator new(sizeof(Singleton));

                        // 2 - constructor
                        new (raw_mem) Singleton();

                        // 3 - assingment of pointer
                        instance_.store(static_cast<Singleton*>(raw_mem));
                    }
                }

                return *instance_;
            }
        };

        atomic<Singleton*> Singleton::instance_;
        mutex Singleton::mtx_;
    }
}

int main()
{
    using namespace EventsWithCv;

    Data data;

    thread thd1{ [&data] { data.read(); } };
    thread thd2{ [&data] { data.process(1); } };
    thread thd3{ [&data] { data.process(2); } };

    thd1.join();
    thd2.join();
    thd3.join();

    using namespace Singletons;

    thread thd4{ [] { Singleton::instance().do_stuff(); } };
    thread thd5{ [] { Singleton::instance().do_stuff(); } };

    thd4.join();
    thd5.join();
}
