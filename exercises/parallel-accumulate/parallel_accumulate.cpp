#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>
#include <future>

namespace Parallel
{

    namespace Deffered
    {

        template <typename It, typename T>
        T parallel_accumulate(It first, It last, T init)
        {
            auto hardware_threads_count = std::max(1u, std::thread::hardware_concurrency());

            std::vector<std::future<T>> partial_results(hardware_threads_count);

            size_t range_size = std::distance(first, last);
            size_t subrange_size = range_size / hardware_threads_count;

            It subrange_end = std::next(first, subrange_size);
            partial_results[0] = std::async(std::launch::deferred,
                                            &std::accumulate<It, T>, first, subrange_end, init);

            for(size_t i = 1; i < partial_results.size(); ++i)
            {
                It subrange_start = subrange_end;
                It subrange_end = (i < hardware_threads_count-1) ? next(subrange_start + subrange_size) : last;

                partial_results[i] = std::async(std::launch::async,
                                                &std::accumulate<It, T>, first, subrange_end, T{});
            }

            return std::accumulate(partial_results.begin(), partial_results.end(), T{},
                                   [](T& item, std::future<T>& f) { return item + f.get(); });
        }
    }

    inline namespace WithoutDeffered
    {
        template <typename It, typename T>
        T parallel_accumulate(It first, It last, T init)
        {
            auto hardware_thread_count = std::max(1u, std::thread::hardware_concurrency());

            std::vector<std::future<T>> partial_results;

            size_t range_size = std::distance(first, last);
            size_t subrange_size = range_size / hardware_thread_count;

            It subrange_start = first;

            for(size_t i = 0; i < hardware_thread_count - 1; ++i)
            {
                It subrange_end = std::next(subrange_start, subrange_size);

                partial_results.push_back(
                            std::async(std::launch::async,
                                       &std::accumulate<It, T>, subrange_start, subrange_end, T{}));

                subrange_start = subrange_end;
            }

            init += std::accumulate(subrange_start, last, T{});

            // reduction
            return std::accumulate(partial_results.begin(), partial_results.end(), init,
                                   [](const T& item, std::future<T>& f) { return item + f.get(); });
        }
    }

}


int main(int argc, char *argv[])
{
    const size_t SIZE = 50000000;

    typedef unsigned long long int ValueType;

    std::vector<ValueType> v(SIZE);
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     std::accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    {
        std::cout << "\nParallel\n";

        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     Parallel::parallel_accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }
}
