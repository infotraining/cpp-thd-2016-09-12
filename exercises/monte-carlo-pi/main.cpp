#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <algorithm>
#include <mutex>
#include <atomic>

using namespace std;

void calc_hits(unsigned long long n, unsigned long long& hits)
{
    random_device rd;
    mt19937_64 rnd_engine{rd()};
    uniform_real_distribution<double> rnd_distr(-1, 1);

    unsigned long long local_hits = 0;

    for(unsigned long long i = 0; i < n; i++)
    {
        double x = rnd_distr(rnd_engine);
        double y = rnd_distr(rnd_engine);

        if(x*x + y*y <= 1)
        {
            local_hits++;
        }
    }

    hits += local_hits;
}

void calc_hits_with_mutex(unsigned long long n, unsigned long long& hits, mutex& mtx)
{
    random_device rd;
    mt19937_64 rnd_engine{rd()};
    uniform_real_distribution<double> rnd_distr(-1, 1);

    for(unsigned long long i = 0; i < n; i++)
    {
        double x = rnd_distr(rnd_engine);
        double y = rnd_distr(rnd_engine);

        if(x*x + y*y <= 1)
        {
            lock_guard<mutex> lk{mtx};
            hits++;            
        }
    }
}

void calc_hits_with_atomic(unsigned long long n, atomic<unsigned long long>& hits)
{
    random_device rd;
    mt19937_64 rnd_engine{rd()};
    uniform_real_distribution<double> rnd_distr(-1, 1);

    for(unsigned long long i = 0; i < n; i++)
    {
        double x = rnd_distr(rnd_engine);
        double y = rnd_distr(rnd_engine);

        if(x*x + y*y <= 1)
        {
            //++hits;
            hits.fetch_add(1, memory_order::memory_order_relaxed);
        }
    }
}

double pi_single_thread(unsigned long long n)
{
    unsigned long long hits = 0;

    calc_hits(n, hits);

    auto pi = 4.0 * hits / n;

    return pi;
}

double pi_multithreading1(unsigned long long n)
{
    const int hardware_threads_count = max(1u, thread::hardware_concurrency());

    vector<thread> threads(hardware_threads_count);
    vector<unsigned long long> partial_hits(hardware_threads_count);

    auto no_of_attempts = n / hardware_threads_count;

    for(int i = 0; i < hardware_threads_count; ++i)
    {
        threads[i] = thread{&calc_hits, no_of_attempts, ref(partial_hits[i])};
    }

    for(auto& t : threads)
        t.join();

    auto hits = accumulate(partial_hits.begin(), partial_hits.end(), 0ull);

    auto pi = 4.0 * hits / n;

    return pi;
}

double pi_multithreading2(unsigned long long n)
{
    const int hardware_threads_count = max(1u, thread::hardware_concurrency());
    vector<thread> threads(hardware_threads_count);

    auto no_of_attempts = n / hardware_threads_count;
    unsigned long long hits = 0;
    mutex mtx_hits;

    for(int i = 0; i < hardware_threads_count; ++i)
    {
        threads[i] = thread{&calc_hits_with_mutex,
                                no_of_attempts, ref(hits), ref(mtx_hits)};
    }

    for(auto& t : threads)
        t.join();

    auto pi = 4.0 * hits / n;

    return pi;

}

double pi_multithreading3(unsigned long long n)
{
    const int hardware_threads_count = max(1u, thread::hardware_concurrency());
    vector<thread> threads(hardware_threads_count);

    auto no_of_attempts = n / hardware_threads_count;
    atomic<unsigned long long> hits{ 0 };

    for(int i = 0; i < hardware_threads_count; ++i)
    {
        threads[i] = thread{&calc_hits_with_atomic,
                                no_of_attempts, ref(hits)};
    }

    for(auto& t : threads)
        t.join();

    auto pi = 4.0 * hits / n;

    return pi;

}



int main()
{
    const int n = 10'000'000;

    cout << "Start single_threaded ver." << endl;

    auto start = chrono::high_resolution_clock::now();

    auto pi = pi_single_thread(n);

    auto end = chrono:: high_resolution_clock::now();

    cout << "Pi: " << pi << " - Time: "
         << chrono::duration_cast<chrono::milliseconds>(end - start).count() << "ms" << endl;

    cout << "\n\n---------------\n\n";

    cout << "Start multithreaded ver. 1" << endl;

    start = chrono::high_resolution_clock::now();

    pi = pi_multithreading1(n);

    end = chrono:: high_resolution_clock::now();

    cout << "Pi: " << pi << " - Time: "
         << chrono::duration_cast<chrono::milliseconds>(end - start).count() << "ms" << endl;

    cout << "\n\n---------------\n\n";

    cout << "Start multithreaded ver. 2 (with mutex)" << endl;

    start = chrono::high_resolution_clock::now();

    pi = pi_multithreading2(n);

    end = chrono:: high_resolution_clock::now();

    cout << "Pi: " << pi << " - Time: "
         << chrono::duration_cast<chrono::milliseconds>(end - start).count() << "ms" << endl;

    cout << "\n\n---------------\n\n";

    cout << "Start multithreaded ver. 3 (with atomic)" << endl;

    start = chrono::high_resolution_clock::now();

    pi = pi_multithreading3(n);

    end = chrono:: high_resolution_clock::now();

    cout << "Pi: " << pi << " - Time: "
         << chrono::duration_cast<chrono::milliseconds>(end - start).count() << "ms" << endl;
}
