#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    using mutex = std::recursive_mutex;

    mutable mutex mtx_;
    const int id_;
    double balance_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_
                  << "; Balance = " << balance() << std::endl;

//        double balance_snapshot;
//        {
//            std::lock_guard<std::mutex> lk{mtx_};
//            balance_snapshot = balance_;

//        }

//        std::cout << balance_ << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<mutex> lk_from{mtx_, std::defer_lock};
        std::unique_lock<mutex> lk_to{to.mtx_, std::defer_lock};

        std::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);

//        balance_ -= amount;
//        to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        std::lock_guard<mutex> lk{mtx_};
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<mutex> lk{mtx_};
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<mutex> lk{mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};




void make_deposit(int counter, BankAccount& ba, double amount)
{
    for(int i = 0; i < counter; ++i)
        ba.deposit(amount);
}

void make_withdraw(int counter, BankAccount& ba, double amount)
{
    for(int i = 0; i < counter; ++i)
        ba.withdraw(amount);
}

void make_transfer(int counter, BankAccount& from, BankAccount& to, double amount)
{
    for(int i = 0; i < counter; ++i)
        from.transfer(to, amount);
}

int main()
{
    using namespace std;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    ba1.print();
    ba2.print();

    cout << "\n\n-------------\n\n";

    constexpr int no_of_operations = 1'000'000;

    thread thd1{make_deposit, no_of_operations, ref(ba1), 1.0};
    thread thd2{make_withdraw, no_of_operations, ref(ba1), 1.0 };

    thd1.join();
    thd2.join();

    ba1.print();
    ba2.print();

    cout << "\n\n-------------\n\n";

    thread thd3{make_transfer, no_of_operations, ref(ba1), ref(ba2), 1.0 };
    thread thd4{make_transfer, no_of_operations, ref(ba2), ref(ba1), 1.0 };

    std::unique_lock<BankAccount> lk{ba1};
    ba1.withdraw(100);
    ba1.withdraw(200);
    ba1.deposit(300);
    lk.unlock();

    thd3.join();
    thd4.join();

    ba1.print();
    ba2.print();
}
