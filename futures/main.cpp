#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>

using namespace std;

void may_throw(int arg)
{
    if (arg % 13 == 0)
        throw runtime_error("Error#13");
}

int calculate_square(int x)
{
    cout << "Starting calculations for " << x << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(1000, 5000);

    auto time_span = chrono::milliseconds(rnd_distr(rnd_gen));
    this_thread::sleep_for(time_span);

    may_throw(x);

    return x * x;
}

void save_to_file(const string& filename)
{
    cout << "Saving to " << filename << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(1000, 5000);

    auto time_span = chrono::milliseconds(rnd_distr(rnd_gen));
    this_thread::sleep_for(time_span);

    cout << "File " << filename << " saved..." << endl;
}

class SquareCalculator
{
    promise<int> promise_;
public:
    void operator()(int x)
    {
        cout << "Starting calculations for " << x << endl;

        random_device rd;
        mt19937_64 rnd_gen(rd());
        uniform_int_distribution<> rnd_distr(1000, 5000);

        auto time_span = chrono::milliseconds(rnd_distr(rnd_gen));
        this_thread::sleep_for(time_span);

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            promise_.set_exception(current_exception());
            return;
        }


        promise_.set_value(x * x);
    }

    future<int> get_future()
    {
        return promise_.get_future();
    }
};

int main()
{
    //1st way - packaged_task
    packaged_task<int()> pt1{ [] { return calculate_square(8); } };
    packaged_task<int(int)> pt2{&calculate_square};
    packaged_task<void()> pt3{ [] { save_to_file("text1.dat"); } };

    future<int> f1 = pt1.get_future();
    future<int> f2 = pt2.get_future();
    future<void> f3 = pt3.get_future();

    thread thd1{move(pt1)};  // async call
    thread thd2{move(pt2), 13};  // async call

    thd1.detach();
    thd2.detach();

    pt3(); // sync call

    while(f1.wait_for(100ms) != future_status::ready)
    {
        cout << "main is waiting for a result from f1" << endl;
    }

    try
    {
        cout << "f1 = " << f1.get() << endl;
        cout << "f2 = " << f2.get() << endl;
        f3.wait();
    }
    catch(const runtime_error& e)
    {
        cout << "Caught an exception: " << e.what() << endl;
    }

    // 2nd - promise
    SquareCalculator square_calc;

    auto f4 = square_calc.get_future();

    thread thd3{move(square_calc), 24};
    thd3.detach();

    cout << "f4 = " << f4.get() << endl;

    // 3rd - async

    future<int> f5 = async(launch::async, &calculate_square, 45);
    future<int> f6 = async(launch::deferred, &calculate_square, 50);

    vector<future<int>> futures;

    futures.push_back(move(f5));
    futures.push_back(move(f6));

    for(int x = 100; x < 120; ++x)
        futures.push_back(async(launch::async, &calculate_square, x));

    for(auto& f : futures)
    {
        try
        {
            cout << "result = " << f.get() << endl;
        }
        catch(const runtime_error& e)
        {
            cout << e.what() << endl;
        }
    }

    cout << "\n\n";

    // C++11 bug

    auto fx1 = async(launch::async, &save_to_file, "f1.txt");
    auto fx2 = async(launch::async, &save_to_file, "f2.txt");
    auto fx3 = async(launch::async, &save_to_file, "f3.txt");
}
