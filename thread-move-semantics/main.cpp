#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds interval)
{
    string text = "Hello threads...";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "End of THD#" << id << endl;
}

thread create_thread()
{
    static int gen_id;

    return thread{&background_work, gen_id, 200ms};
}

int main()
{
    auto hardware_threads_count = max(1u, thread::hardware_concurrency());

    cout << "hardware_threads_count: " << hardware_threads_count << endl;

    vector<thread> threads;

    thread thd1{&background_work, 0, 500ms};

    threads.push_back(move(thd1));
    threads.push_back(create_thread());
    threads.push_back(thread{&background_work, 100, 300ms});
    threads.emplace_back(&background_work, 200, 150ms);

    for(auto& t : threads)
        t.join();
}
