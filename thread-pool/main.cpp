#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>
#include <random>
#include <future>

#include "thread_safe_queue.hpp"

using namespace std;

using Task = function<void()>;

class ThreadPool
{
public:
    ThreadPool(size_t size) : threads_(size)
    {
        for(size_t i = 0; i < size; ++i)
        {
            threads_[i] = thread{ [this] { run(); } };
        }
    }

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            tasks_.push(end_of_work_);

        for(auto& t : threads_)
            t.join();
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    template <typename Callable>
    auto submit(Callable&& task) -> future<decltype(task())>
    {
        using result_type = decltype(task());

        auto pt = make_shared<packaged_task<result_type()>>(forward<Callable>(task));
        auto future_result = pt->get_future();

        tasks_.push([pt] { (*pt)(); });

        return future_result;
    }

private:
    vector<thread> threads_;
    ThreadSafeQueue<Task> tasks_;
    static const nullptr_t end_of_work_;

    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);

            if (task == end_of_work_)
                return;

            task(); // execution of task
        }
    }
};

void background_work(int id)
{
    cout << "BW#" << id << " starts..." << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(1000, 5000);

    auto time_span = chrono::milliseconds(rnd_distr(rnd_gen));
    this_thread::sleep_for(time_span);

    cout << "BW#" << id << "ends..." << endl;
}

void may_throw(int arg)
{
    if (arg % 13 == 0)
        throw runtime_error("Error#13");
}

int calculate_square(int x)
{
    cout << "Starting calculations for " << x << endl;

    random_device rd;
    mt19937_64 rnd_gen(rd());
    uniform_int_distribution<> rnd_distr(1000, 5000);

    auto time_span = chrono::milliseconds(rnd_distr(rnd_gen));
    this_thread::sleep_for(time_span);

    may_throw(x);

    return x * x;
}

int main()
{
    ThreadPool thread_pool{8};

    thread_pool.submit([]{ background_work(1);});
    thread_pool.submit([]{ background_work(2);});

    for(int i = 3; i <= 20; ++i)
        thread_pool.submit([i] { background_work(i);});

    vector<future<int>> squares;

    for(size_t i = 100; i <= 140; ++i)
        squares.push_back(thread_pool.submit([i] { return calculate_square(i);}));

    cout << "\n\nSquares: \n";

    for(auto& fs : squares)
    {
        try
        {
            auto result = fs.get();
            cout << "result = " << result << endl;
        }
        catch(const runtime_error& e)
        {
            cout << e.what() << endl;
        }
    }
}

