#ifndef THREAD_SAFE_QUEUE_HPP
#define THREAD_SAFE_QUEUE_HPP

#include <queue>
#include <mutex>
#include <condition_variable>

template <typename T>
class ThreadSafeQueue
{
    std::queue<T> q_;
    mutable std::mutex mtx_;
    std::condition_variable cv_not_empty_;
public:
    ThreadSafeQueue() = default;

    ThreadSafeQueue(const ThreadSafeQueue&) = delete;
    ThreadSafeQueue& operator=(const ThreadSafeQueue&) = delete;

    bool empty() const
    {
        std::lock_guard<std::mutex> lk{mtx_};
        return q_.empty();
    }

    void push(const T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_};
        q_.push(item);
        lk.unlock();

        cv_not_empty_.notify_one();
    }

    void push(T&& item)
    {
        std::unique_lock<std::mutex> lk{mtx_};
        q_.push(std::move(item));
        lk.unlock();

        cv_not_empty_.notify_one();
    }

    void push(std::initializer_list<T> items)
    {
        std::unique_lock<std::mutex> lk{mtx_};

        for(const auto& item : items)
            q_.push(item);
        lk.unlock();

        cv_not_empty_.notify_all();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lk{mtx_};

        cv_not_empty_.wait(lk, [this] { return !q_.empty();});

        item = q_.front();
        q_.pop();
    }

    bool try_pop(T& item)
    {
        std::lock_guard<std::mutex> lk{mtx_};

        if (q_.empty())
            return false;

        item = q_.front();
        q_.pop();

        return true;
    }

};

#endif // THREAD_SAFE_QUEUE_HPP
