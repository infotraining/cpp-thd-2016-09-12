#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13 from THD#" + to_string(id));
    else if (arg == 42)
        throw invalid_argument("Error#42 from THD#" + to_string(id));
}

void background_work(int id, int count, chrono::milliseconds interval,
                     exception_ptr& eptr)
{
    cout << "Start of THD#" << id << endl;

    try
    {
        for(int i = 0; i < count; ++i)
        {
            cout << "THD#" << id << ": " << i << endl;
            this_thread::sleep_for(interval);

            may_throw(id, id % 2 ? i : i*2);
        }
    }
    catch(...)
    {
        eptr = current_exception();
    }

    cout << "End of THD#" << id << endl;
}

int main() try
{
    auto start = chrono::high_resolution_clock::now();

    exception_ptr eptr;

    thread thd1{&background_work, 2, 30, 200ms, ref(eptr)};
    thd1.join();

    // error handling
    if (eptr)
    {
        try
        {
            rethrow_exception(eptr);
        }
        catch(const runtime_error& e)
        {
            cout << "Handling runtime error: " << e.what() << endl;
        }
        catch(const invalid_argument& e)
        {
            cout << "Handling invalid arg: " << e.what() << endl;
        }
    }


    // many threads

    const int no_of_threads = 10;

    vector<thread> threads(no_of_threads);
    vector<exception_ptr> eptrs(no_of_threads);

    for(int i = 0; i < no_of_threads; ++i)
    {
        threads[i] = thread{&background_work, i, i *5, 100ms, ref(eptrs[i])};
    }

    for(auto& t : threads)
        t.join();

    for(auto& eptr : eptrs)
    {
        if (eptr)
        {
            try
            {
                rethrow_exception(eptr);
            }
            catch(const runtime_error& e)
            {
                cout << "Handling runtime error: " << e.what() << endl;
            }
            catch(const invalid_argument& e)
            {
                cout << "Handling invalid arg: " << e.what() << endl;
            }
        }
    }

    auto end = chrono::high_resolution_clock::now();

    auto time_span = chrono::duration_cast<chrono::milliseconds>(end - start).count();

    cout << "Time: " << time_span << "ms" << endl;

}
catch(const exception& e)
{
    cout << "Caught an exception: " << e.what() << endl;
}
