#include <iostream>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

using namespace std;

void background_work(int id, chrono::milliseconds interval)
{
    string text = "Hello threads...";

    for(const auto& c : text)
    {
        cout << "THD#" << id << ": " << c << endl;
        this_thread::sleep_for(interval);
    }

    cout << "End of THD#" << id << endl;
}

void may_throw()
{
    throw runtime_error("Error#13");
}

namespace util
{
    namespace with_ref
    {
        class RaiiThread
        {
            thread& thd_;
        public:
            RaiiThread(thread& thd) : thd_{thd}
            {}

            RaiiThread(const RaiiThread&) = delete;
            RaiiThread& operator=(const RaiiThread&) = delete;

            thread& get()
            {
                return thd_;
            }

            ~RaiiThread()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };
    }

    inline namespace with_move_semantics
    {
        class RaiiThread
        {
        private:
            thread thd_;

        public:
            RaiiThread(thread&& thd) : thd_(move(thd))
            {}

            template <typename... Args>
            RaiiThread(Args... args) : thd_{forward<Args>(args)...}
            {
            }

            RaiiThread(const RaiiThread&) = delete;
            RaiiThread& operator=(const RaiiThread&) = delete;

            RaiiThread(RaiiThread&&) = default;
            RaiiThread& operator=(RaiiThread&&) = default;

            thread& get()
            {
                return thd_;
            }

            ~RaiiThread()
            {
                if (thd_.joinable())
                    thd_.join();
            }
        };

    }

}

int main() try
{
    thread thd1{&background_work, 1, 200ms};

    util::RaiiThread thd1_guard{move(thd1)};
    util::RaiiThread thd2_guard{&background_work, 2, 300ms};

    may_throw();
}
catch(const runtime_error& e)
{
    cout << "Caught an exception: " << e.what() << endl;
}
